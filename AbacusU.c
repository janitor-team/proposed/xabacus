/*
 * @(#)AbacusU.c
 *
 * Copyright 2020  David A. Bagley, bagleyd AT verizon.net
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Undo algorithm for Abacus */

#include "AbacusP.h"

#define SYMBOL ':'
#define MAX_AUX 3

/* int *startLoc[MAX_AUX][MAX_DECKS]; */

static void
newStack(AbacusStack *s)
{
	if (s->lastMove != NULL || s->firstMove != NULL)
		return;
	if (!(s->lastMove = (MoveStack *) malloc(sizeof (MoveStack)))) {
		DISPLAY_ERROR("Not enough memory, exiting.");
	}
	if (!(s->firstMove = (MoveStack *) malloc(sizeof (MoveStack)))) {
		DISPLAY_ERROR("Not enough memory, exiting.");
	}
	s->firstMove->previous = s->lastMove->next = NULL;
	s->firstMove->next = s->lastMove;
	s->lastMove->previous = s->firstMove;
	s->count = 0;
}

static void
pushStack(AbacusStack *s, MoveRecord **move)
{
	if (!(s->currMove = (MoveStack *) malloc(sizeof (MoveStack)))) {
		DISPLAY_ERROR("Not enough memory, exiting.");
	}
	s->lastMove->previous->next = s->currMove;
	s->currMove->previous = s->lastMove->previous;
	s->currMove->next = s->lastMove;
	s->lastMove->previous = s->currMove;
	*move = &(s->currMove->move);
	s->count++;
}

static void
popStack(AbacusStack *s)
{
	s->currMove = s->lastMove->previous;
	s->lastMove->previous->previous->next = s->lastMove;
	s->lastMove->previous = s->lastMove->previous->previous;
	free(s->currMove);
	s->count--;
}

static MoveRecord *
topStack(AbacusStack *s)
{
	return &(s->lastMove->previous->move);
}

static int
emptyStack(AbacusStack *s)
{
	return (s->lastMove->previous == s->firstMove);
}

static void
flushStack(AbacusStack *s)
{
	while (s->lastMove->previous != s->firstMove) {
		s->currMove = s->lastMove->previous;
		s->lastMove->previous->previous->next = s->lastMove;
		s->lastMove->previous = s->lastMove->previous->previous;
		free(s->currMove);
	}
	s->count = 0;
}

static void
deleteStack(AbacusStack *s)
{
	flushStack(s);
	if (s->firstMove) {
		free(s->firstMove);
		s->firstMove = NULL;
	}
	if (s->lastMove) {
		free(s->lastMove);
		s->lastMove = NULL;
	}
}

/**********************************/

void
newMoves(AbacusStack *s)
{
	newStack(s);
}

void
deleteMoves(AbacusStack *s)
{
	deleteStack(s);
}

static void
writeMove(MoveRecord * move, int aux, int deck, int rail, int number)
{
	move->aux = aux;
	move->deck = deck;
	move->rail = rail;
	move->number = number;
}

static void
readMove(MoveRecord *move, int *aux, int *deck, int *rail, int *number)
{
	*aux = move->aux;
	*deck = move->deck;
	*rail = move->rail;
	*number = move->number;
}

void
setMove(AbacusStack *s, int aux, int deck, int rail, int number)
{
        MoveRecord *move;

#ifdef DEBUG
	(void) printf("setMove: %d, %d, %d, %d\n",
		aux, deck, rail, number);
#endif
        pushStack(s, &move);
        writeMove(move, aux, deck, rail, number);
}

void
getMove(AbacusStack *s, int *aux, int *deck, int *rail, int *number)
{
	readMove(topStack(s), aux, deck, rail, number);
	popStack(s);
}

int
madeMoves(AbacusStack *s)
{
	return !emptyStack(s);
}

void
flushMoves(AbacusStack *s)
{
	flushStack(s);
}

int
numMoves(AbacusStack *s)
{
	return s->count;
}

void
scanMoves(FILE *fp, AbacusWidget w, int moves)
{
	int aux, deck, rail, number, l, c;

	for (l = 0; l < moves; l++) {
		while ((c = getc(fp)) != EOF && c != SYMBOL);
		if (fscanf(fp, "%d %d %d %d", &aux, &deck, &rail, &number) != 4)
			(void) fprintf(stderr,
				"corrupt scan, expecting 4 integers for move %d\n", l);
#ifdef DEBUG
		(void) printf("%d %d %d %d", aux, deck, rail, number);
#endif
		setAbacusMove(w, ACTION_SCRIPT, aux, deck, rail, number);
	}
}

void
printMoves(FILE *fp, AbacusStack *s)
{
	int aux, deck, rail, number, counter = 0;

	s->currMove = s->firstMove->next;
	(void) fprintf(fp, "moves\taux\tdeck\trail\tnumber\n");
	while (s->currMove != s->lastMove) {
		readMove(&(s->currMove->move), &aux, &deck, &rail, &number);
		(void) fprintf(fp, "%d%c\t%d\t%d\t%d\t%d\n", ++counter, SYMBOL, aux, deck, rail, number);
		s->currMove = s->currMove->next;
	}
}

/*void
scanStartPosition(FILE *fp, AbacusWidget w)
{
	int position, stack, c;

	while ((c = getc(fp)) != EOF && c != SYMBOL);
	for (position = 0; position < w->abacus.tiles; position++)
		for (stack = 0; stack < w->abacus.stacks; stack++) {
			if (fscanf(fp, "%d %d",
					&(startLoc[stack][position].stack),
					&(startLoc[stack][position].loc)) != 2)
				(void) fprintf(stderr,
					"corrupt start record, expecting 2 integers for %d %d\n",
					position, stack);
#ifdef DEBUG
			(void) printf("%d %d\n",
				startLoc[stack][position].stack,
				startLoc[stack][position].loc);
#endif
		}
}

void
printStartPosition(FILE *fp, AbacusWidget w)
{
	int position, stack;

	(void) fprintf(fp, "\nstartingPosition%c\n", SYMBOL);
	for (position = 0; position < w->abacus.tiles; position++) {
		for (stack = 0; stack < w->abacus.stacks; stack++) {
			(void) fprintf(fp, "%4d%3d",
				startLoc[stack][position].stack,
				startLoc[stack][position].loc);
		}
		(void) fprintf(fp, "\n");
	}
	(void) fprintf(fp, "\n");
}

void
setStartPosition(AbacusWidget w)
{
	int i, j;

	for (i = 0; i < MAX_STACKS; i++)
		for (j = 0; j <= w->abacus.tiles; j++)
			w->abacus.tileOfPosition[i][j].stack = -1;
	for (i = 0; i < w->abacus.stacks; i++)
		for (j = 0; j < w->abacus.tiles; j++) {
			w->abacus.positionOfTile[i][j] = startLoc[i][j];
			w->abacus.tileOfPosition[startLoc[i][j].stack][startLoc[i][j].loc].stack =
				i;
			w->abacus.tileOfPosition[startLoc[i][j].stack][startLoc[i][j].loc].loc =
				j;
		}
	drawAllTiles(w);
}*/

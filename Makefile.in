# @(#)Makefile.in
#
# Copyright 1997 - 2020  David A. Bagley, bagleyd AT verizon.net
#
# All Rights Reserved
#
# Permission to use, copy, modify, and distribute this software and
# its documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and
# that both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of the author not be
# used in advertising or publicity pertaining to distribution of the
# software without specific, written prior permission.
#
# This program is distributed in the hope that it will be "useful",
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# xabacus Makefile.in for configure (UNIX X11 support)

wIDGET = abacus
WIDGET = Abacus
GROUP = xabacus
P=x
R=$(P)
V=
PROG = $(R)$(wIDGET)
VER = $(V)$(wIDGET)

# this tells GNU make not to export variables into the environment
# But other makes do not understand its significance, so it must
# not be the first target in the file. So it is here, before
# any variables are created, but after the default target
.NOEXPORT :

SHELL = /bin/sh
# Its not a game, so you may not want this.  Man page itself is hard coded.
MANNUM = 6
#MANNUM = 1

srcdir = @srcdir@
top_srcdir = @top_srcdir@
VPATH = @srcdir@

prefix = @prefix@
exec_prefix = @exec_prefix@

bindir = @bindir@
mandir = @mandir@/man$(MANNUM)
xapploaddir = @APPDEFAULTS@
#xapploaddir = @libdir@/X11/app-defaults
#xapploaddir = @libdir@/app-defaults
datarootdir = @datarootdir@
datadir = @datadir@
readdir = $(datadir)/games/$(GROUP)
#gettextdir = /usr/share/locale
gettextdir = @srcdir@

INSTALL = @INSTALL@
INSTALL_DATA = @INSTALL_DATA@
INSTALL_PROGRAM = @INSTALL_PROGRAM@
INSTALL_SCRIPT = @INSTALL_SCRIPT@
M = @XM_PREFIX@

DESTDIR =

#CC = cc -g
#CC = acc -g
#CC = gcc -g -Wall -ansi -pedantic
#CC = gcc -g -Wall -W -Wshadow -Wpointer-arith -Wbad-function-cast -Wcast-align -Wwrite-strings -Waggregate-return -Wmissing-prototypes -Wstrict-prototypes -pedantic -Wno-long-long
#CC = gcc -std=c89
#CC = gcc -g -Wall
#CC = g++ -g -Wall
#CC = CC -g
CC = @CC@

LINT = lint
#LINT = alint

INDENT = indent

MORE = more
#MORE = less

PRINT = lpr
#PRINT = enscript -2r

LN_S = @LN_S@
RM = rm -f
RM_S = $(RM)
ECHO = echo

#BLN_S = set file/enter=[]
#RM = delete/noconfirm/nolog
#RM_S = set file/remove/nolog
#ECHO = write sys$output

# wav files in win32 may sound better
W = @SOUNDEXT@

# Assumes a directory of /usr/local/share/games/$(GROUP) if -DDEMOPATH not set.
DEFINES = \
-DSOUNDPATH=\"$(readdir)\" -DSOUNDEXT=\"$(W)\" \
-DBUMPSOUND=\"bump\" -DMOVESOUND=\"move\" -DDRIPSOUND=\"drip\" \
-DDEMOPATH=\"$(readdir)\" -DUSE_SPIN -DGETTEXTPATH=\"$(gettextdir)\"
DEFS = @DEFS@ $(DEFINES)
# -DDEBUG
# -DSRAND=srand48 -DLRAND=lrand48 -DMAXRAND=2147483648.0
# -DSRAND=srandom -DLRAND=random -DMAXRAND=2147483648.0
# -DSRAND=srand -DLRAND=rand -DMAXRAND=32768.0
# -DHAVE_USLEEP
# -DHAVE_NANOSLEEP
# -DCONSTPIXMAPS
XWIDGETINC = @XWIDGETINC@ -I$(top_srcdir) -I.
CFLAGS = @CFLAGS@
#CFLAGS = -O
#CFLAGS = -g
XWIDGETLDFLAGS = @XWIDGETLDFLAGS@
XLIBS = @XLIBS@
XWIDGETLIBS = @XWIDGETLIBS@ -lm

N=
C=.c
#C++
#C=.cc
#C=.cpp

O=.o
#O=.@OBJEXT@
S=$(N) $(N)
E=@EXEEXT@
A=

#VMS
#O=.obj
#S=,
#E=.exe
#A=;*

# please define
# C as the C source code extension
# O as the object extension
# S as the separator for object code
# E as the executable extension

HDRS = $(WIDGET)P.h $(WIDGET).h \
xwin.h file.h timer.h rngs.h \
sound.h version.h pixmaps/64x64/$(wIDGET).xbm \
pixmaps/16x16/$(wIDGET).xpm pixmaps/22x22/$(wIDGET).xpm \
pixmaps/24x24/$(wIDGET).xpm pixmaps/32x32/$(wIDGET).xpm \
pixmaps/48x48/$(wIDGET).xpm pixmaps/64x64/$(wIDGET).xpm

COREOBJS = $(WIDGET)$(O)$(S)$(WIDGET)U$(O)$(S)$(WIDGET)C$(O)$(S)\
$(WIDGET)D$(O)$(S)$(WIDGET)M$(O)$(S)$(WIDGET)T$(O)$(S)$(WIDGET)F$(O)$(S)\
$(WIDGET)E$(O)
UTILOBJS = xwin$(O)$(S)file$(O)$(S)timer$(O)$(S)rngs$(O)$(S)sound$(O)
OBJS = $(UTILOBJS)$(S)$(COREOBJS)$(S)$(P)$(wIDGET)$(O)

CORESRCS = $(WIDGET)$(C) $(WIDGET)U$(C) $(WIDGET)C$(C) \
$(WIDGET)D$(C) $(WIDGET)M$(C) $(WIDGET)T$(C) $(WIDGET)F$(C) \
$(WIDGET)E$(C)
UTILSRCS = xwin$(C) file$(C) timer$(C) rngs$(C) sound$(C)
SRCS = $(UTILSRCS) $(CORESRCS) $(P)$(wIDGET)$(C)

all : $(PROG)

constpixmaps :
	@ $(ECHO) "USE with -DCONSTPIXMAPS"
	for pix in pixmaps/*/*.xpm; do\
		base=`basename $$pix`;\
		sed 's/static char/static const char/' $$pix > $$base;\
	done

$(PROG) : $(OBJS)
	$(CC) -o $@ $(OBJS) $(XWIDGETLDFLAGS) $(XWIDGETLIBS)
	@ $(ECHO) "$@ BUILD COMPLETE"
	@ $(ECHO) ""
	@ $(ECHO) "To install: \"make install\" is safer but"
	@ $(ECHO) "\"make install-games\" may work better, see INSTALL."

$(WIDGET)$(O) : $(WIDGET)$(C) $(HDRS)
$(WIDGET)U$(O) : $(WIDGET)U$(C) $(WIDGET)P.h $(WIDGET).h
$(WIDGET)C$(O) : $(WIDGET)C$(C) $(WIDGET)P.h $(WIDGET).h
$(WIDGET)D$(O) : $(WIDGET)D$(C) $(WIDGET)P.h $(WIDGET).h file.h
$(WIDGET)M$(O) : $(WIDGET)M$(C) $(WIDGET)P.h $(WIDGET).h
$(WIDGET)T$(O) : $(WIDGET)T$(C) $(WIDGET)P.h $(WIDGET).h
$(WIDGET)F$(O) : $(WIDGET)F$(C) $(WIDGET)P.h
$(WIDGET)E$(O) : $(WIDGET)E$(C) $(WIDGET)P.h
xwin$(O) : xwin$(C) xwin.h
file$(O) : file$(C) file.h
timer$(O) : timer$(C) timer.h
rngs$(O) : rngs$(C) rngs.h
sound$(O) : sound$(C) sound.h file.h
$(P)$(wIDGET)$(O) : $(P)$(wIDGET)$(C) $(WIDGET).h xwin.h file.h version.h

install : $(PROG)$(E) @NOPLAY@play.sh
	$(srcdir)/mkinstalldirs $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) $(PROG)$(E) $(DESTDIR)$(bindir)/$(R)$(M)$(wIDGET)$(E)
	@NOPLAY@$(INSTALL_SCRIPT) play.sh $(DESTDIR)$(bindir)/play.sh
	$(srcdir)/mkinstalldirs $(DESTDIR)$(mandir)
	$(INSTALL_DATA) $(srcdir)/$(PROG).man $(DESTDIR)$(mandir)/$(PROG).$(MANNUM)
	$(srcdir)/mkinstalldirs $(DESTDIR)$(xapploaddir)
	sed 's;^$(WIDGET)\*\(.*\): /usr/local/share/games/$(GROUP);$(WIDGET)*\1: $(DESTDIR)$(readdir);g' $(srcdir)/$(WIDGET).ad > $(WIDGET).ad.tmp
	$(INSTALL_DATA) $(WIDGET).ad.tmp $(DESTDIR)$(xapploaddir)/$(WIDGET)
	rm -f $(WIDGET).ad.tmp
	$(srcdir)/mkinstalldirs $(DESTDIR)$(readdir)
	$(INSTALL_DATA) $(srcdir)/$(WIDGET).ps $(DESTDIR)$(readdir)/$(WIDGET).ps
	$(INSTALL_DATA) $(srcdir)/abacusDemo.xml $(DESTDIR)$(readdir)/abacusDemo.xml
	$(INSTALL_DATA) $(srcdir)@WAVDIR@/bump$(W) $(DESTDIR)$(readdir)/bump$(W)
	$(INSTALL_DATA) $(srcdir)@WAVDIR@/move$(W) $(DESTDIR)$(readdir)/move$(W)
	$(INSTALL_DATA) $(srcdir)@WAVDIR@/drip$(W) $(DESTDIR)$(readdir)/drip$(W)
	@ $(ECHO) "$@ COMPLETE"
	@ $(ECHO) ""
	@ $(ECHO) "To use $(R)$(M)$(wIDGET) from a menu, you may want to install the"
	@ $(ECHO) "images.  Do a \"make install-png\" for Gnome and KDE, or do a"
	@ $(ECHO) "\"make install-xpm\" or a \"make install-xpm-home\" for CDE."

uninstall :
	$(RM) $(DESTDIR)$(bindir)/$(R)$(M)$(wIDGET)$(E)
	@NOPLAY@$(RM) $(DESTDIR)$(bindir)/play.sh
	$(RM) $(DESTDIR)$(mandir)/$(PROG).$(MANNUM)
	$(RM) $(DESTDIR)$(xapploaddir)/$(WIDGET)
	$(RM) $(DESTDIR)$(readdir)/$(WIDGET).ps
	$(RM) $(DESTDIR)$(readdir)/abacusDemo.xml
	$(RM) $(DESTDIR)$(readdir)/bump$(W)
	$(RM) $(DESTDIR)$(readdir)/move$(W)
	$(RM) $(DESTDIR)$(readdir)/drip$(W)

install-games : $(PROG)$(E) @NOPLAY@play.sh
	$(srcdir)/mkinstalldirs /usr/games
	$(INSTALL_PROGRAM) $(PROG)$(E) /usr/games/$(R)$(M)$(wIDGET)$(E)
	@NOPLAY@$(INSTALL_SCRIPT) play.sh $(DESTDIR)$(bindir)/play.sh
	chmod 2755 /usr/games/$(R)$(M)$(wIDGET)$(E)
	chown games:games /usr/games/$(R)$(M)$(wIDGET)$(E)
	$(srcdir)/mkinstalldirs $(DESTDIR)$(mandir)
	$(INSTALL_DATA) $(srcdir)/$(PROG).man $(DESTDIR)$(mandir)/$(PROG).$(MANNUM)
	$(srcdir)/mkinstalldirs $(DESTDIR)$(xapploaddir)
	sed 's;^$(WIDGET)\*\(.*\): /usr/local/share/games/$(GROUP);$(WIDGET)*\1: $(DESTDIR)$(readdir);g' $(srcdir)/$(WIDGET).ad > $(WIDGET).ad.tmp
	$(INSTALL_DATA) $(WIDGET).ad.tmp $(DESTDIR)$(xapploaddir)/$(WIDGET)
	rm -f $(WIDGET).ad.tmp
	$(srcdir)/mkinstalldirs $(DESTDIR)$(readdir)
	$(INSTALL_DATA) $(srcdir)/$(WIDGET).ps $(DESTDIR)$(readdir)/$(WIDGET).ps
	$(INSTALL_DATA) $(srcdir)/abacusDemo.xml $(DESTDIR)$(readdir)/abacusDemo.xml
	$(INSTALL_DATA) $(srcdir)@WAVDIR@/bump$(W) $(DESTDIR)$(readdir)/bump$(W)
	$(INSTALL_DATA) $(srcdir)@WAVDIR@/move$(W) $(DESTDIR)$(readdir)/move$(W)
	$(INSTALL_DATA) $(srcdir)@WAVDIR@/drip$(W) $(DESTDIR)$(readdir)/drip$(W)
	@ $(ECHO) "$@ COMPLETE"
	@ $(ECHO) ""
	@ $(ECHO) "To use $(R)$(M)$(wIDGET) from a menu, you may want to install the "
	@ $(ECHO) "images.  Do a \"make install-png\" for Gnome and KDE, or do a"
	@ $(ECHO) "\"make install-xpm\" or a \"make install-xpm-home\" for CDE."

uninstall-games :
	$(RM) /usr/games/$(R)$(M)$(wIDGET)$(E)
	@NOPLAY@$(RM) $(DESTDIR)$(bindir)/play.sh
	$(RM) $(DESTDIR)$(mandir)/$(PROG).$(MANNUM)
	$(RM) $(DESTDIR)$(xapploaddir)/$(WIDGET)
	$(RM) $(DESTDIR)$(readdir)/$(WIDGET).ps
	$(RM) $(DESTDIR)$(readdir)/abacusDemo.xml
	$(RM) $(DESTDIR)$(readdir)/bump$(W)
	$(RM) $(DESTDIR)$(readdir)/move$(W)
	$(RM) $(DESTDIR)$(readdir)/drip$(W)

# Gnome and KDE
install-png :
	for i in 16x16 22x22 32x32 48x48; do\
		$(srcdir)/mkinstalldirs $(DESTDIR)$(datadir)/pixmaps/png/hicolor/$$i/apps;\
		$(INSTALL_DATA) pixmaps/$$i/$(wIDGET).png $(DESTDIR)$(datadir)/pixmaps/png/hicolor/$$i/apps/$(wIDGET).png;\
	done
	$(srcdir)/mkinstalldirs $(DESTDIR)$(datadir)/pixmaps/png/hicolor/all/apps
	$(LN_S) $(DESTDIR)$(datadir)/pixmaps/png/hicolor/16x16/apps/$(wIDGET).png $(DESTDIR)$(datadir)/pixmaps/png/hicolor/all/apps/mini.$(wIDGET).png
	$(LN_S) $(DESTDIR)$(datadir)/pixmaps/png/hicolor/22x22/apps/$(wIDGET).png $(DESTDIR)$(datadir)/pixmaps/png/hicolor/all/apps/small.$(wIDGET).png
	$(LN_S) $(DESTDIR)$(datadir)/pixmaps/png/hicolor/32x32/apps/$(wIDGET).png $(DESTDIR)$(datadir)/pixmaps/png/hicolor/all/apps/normal.$(wIDGET).png
	$(LN_S) $(DESTDIR)$(datadir)/pixmaps/png/hicolor/48x48/apps/$(wIDGET).png $(DESTDIR)$(datadir)/pixmaps/png/hicolor/all/apps/big.$(wIDGET).png

uninstall-png :
	for i in mini small normal big; do\
		$(RM) $(DESTDIR)$(datadir)/pixmaps/png/hicolor/all/apps/$$i.$(wIDGET).png;\
	done
	for i in 16x16 22x22 32x32 48x48; do\
		$(RM) $(DESTDIR)$(datadir)/pixmaps/png/hicolor/$$i/apps/$(wIDGET).png;\
	done

#CDE
install-xpm :
	$(srcdir)/mkinstalldirs $(DESTDIR)$(datadir)/dt/pixmaps
	$(INSTALL_DATA) pixmaps/16x16/$(wIDGET).xpm $(DESTDIR)$(datadir)/dt/pixmaps/$(wIDGET).t.pm
	$(INSTALL_DATA) pixmaps/24x24/$(wIDGET).xpm $(DESTDIR)$(datadir)/dt/pixmaps/$(wIDGET).s.pm
	$(INSTALL_DATA) pixmaps/32x32/$(wIDGET).xpm $(DESTDIR)$(datadir)/dt/pixmaps/$(wIDGET).m.pm
	$(INSTALL_DATA) pixmaps/48x48/$(wIDGET).xpm $(DESTDIR)$(datadir)/dt/pixmaps/$(wIDGET).l.pm

uninstall-xpm :
	for i in t s m l; do\
		$(RM) $(DESTDIR)$(datadir)/dt/pixmaps/$(wIDGET).$$i.pm;\
	done

#CDE HOME directory
install-xpm-home :
	$(srcdir)/mkinstalldirs $(HOME)/.dt/pixmaps
	$(INSTALL_DATA) pixmaps/16x16/$(wIDGET).xpm $(HOME)/.dt/pixmaps/$(wIDGET).t.pm
	$(INSTALL_DATA) pixmaps/24x24/$(wIDGET).xpm $(HOME)/.dt/pixmaps/$(wIDGET).s.pm
	$(INSTALL_DATA) pixmaps/32x32/$(wIDGET).xpm $(HOME)/.dt/pixmaps/$(wIDGET).m.pm
	$(INSTALL_DATA) pixmaps/64x64/$(wIDGET).xpm $(HOME)/.dt/pixmaps/$(wIDGET).l.pm

uninstall-xpm-home :
	for i in t s m l; do\
		$(RM) $(HOME)/.dt/pixmaps/$(wIDGET).$$i.pm;\
	done

SUFFIXES : $(C) $(O) $(E)

$(C)$(O) :
	$(CC) -c $(CPPFLAGS) $(DEFS) $(XWIDGETINC) $(CFLAGS) $<

Makefile : Makefile.in config.status
	$(SHELL) config.status
config.status : configure
	$(SHELL) config.status --recheck
#configure : configure.ac
# enable this rule if you want autoconf to be executed automatically when
# configure.ac is changed. This is commented out, since patching might give
# configure.ac a newer timestamp than configure and not everybody has autoconf
#	cd $(srcdir); autoconf

run :
	./$(PROG)

run.scores :

run.version :
	./$(PROG) -version

lee :
	./$(PROG) -lee -leftAuxRails 9 -rightAuxRails 9

nolee :
	./$(PROG) -nolee -rails 15

#Roman Hand Abacus (right most column twelfths and ancient Roman Numerals in display)
roman :
	./$(PROG) -nolee -roman -topPiece 2 -bottomPiece 6 -subdeck 3 -rails 10 -decimalPosition 2 -romanNumerals -ancientRoman -nomodernRoman -nolatin

#Alt Roman Hand Abacus (right most column eighths and modern Roman Numerals on abacus)
roman8 :
	./$(PROG) -nolee -roman -subdeck 3 -topPiece 2 -bottomPiece 6 -rails 10 -decimalPosition 2 -romanNumerals -noancientRoman -modernRoman -eighth -nolatin

#Russian Schoty
russian :
	./$(PROG) -nolee -russian -bottomPiece 4 -rails 11

#Old Russian Schoty
russianold :
	./$(PROG) -nolee -russian -bottomPiece 4 -bottomPiecePercent 4 -rails 11

#Georgian Schoty (not to be taken seriously)
georgian :
	./$(PROG) -nolee -russian -bottomPiece 4 -bottomPiecePercent 4 -rails 11 -base 20

#Danish School Abacus
danish :
	./$(PROG) -nolee -danish -rails 10 -decimalPosition 0 -decimalComma -group

#Medieval Counter
medieval :
	./$(PROG) -medieval -rails 5 -decimalPosition 0 -romanNumerals

#Mesoamerican Nepohualtzintzin (similar to Japanese Soroban base 20)
mesoamerican :
	./$(PROG) -nolee -generic -nodiamond -topNumber 3 -bottomNumber 4 -topSpaces 1 -bottomSpaces 1 -decimalPosition 0 -base 20 -anomaly 2 -novertical -colorScheme 8 -secondaryBeadColor darkBlue

#Babylonian Watch (proposed by author)
watch :
	./$(PROG) -nolee -generic -slot -topNumber 1 -bottomNumber 1 -topSpaces 1 -bottomSpaces 1 -anomaly 4 -anomalySq 4

#Chinese solid-and-broken-bar system
bar :
	./$(PROG) -nolee -generic -topFactor 3 -topNumber 3 -bottomNumber 2 -decimalPosition 0 -base 12 -displayBase 12

#Chinese base 16 using normal abacus
cn16 :
	./$(PROG) -nolee -generic -topFactor 5 -topNumber 2 -bottomNumber 5 -decimalPosition 0 -base 16 -displayBase 16

cn17 :
	./$(PROG) -nolee -generic -topFactor 5 -topNumber 2 -bottomNumber 6 -decimalPosition 0 -base 17 -displayBase 17

ko11 :
	./$(PROG) -nolee -generic -topFactor 5 -topNumber 1 -bottomNumber 5 -decimalPosition 0 -base 11 -displayBase 11 -topSpaces 1 -bottomSpaces 1 -diamond

jp9 :
	./$(PROG) -nolee -generic -topFactor 4 -topNumber 1 -bottomNumber 4 -decimalPosition 0 -base 9 -displayBase 9 -topSpaces 1 -bottomSpaces 1 -diamond

#Base 16 Abacus
base16 :
#	./$(PROG) -nolee -japanese -base 16 -displayBase 10
	./$(PROG) -nolee -japanese -base 16 -displayBase 16

percent :
	./$(PROG) -bottomPiecePercent 4

lldb :
	lldb ./$(PROG)

gdb :
	gdb ./$(PROG)

dbx :
	dbx ./$(PROG)

valgrind :
	valgrind --track-origins=yes --leak-check=full --show-leak-kinds=all \
./$(PROG)

clean :
	$(RM) *.o *.exe* core *~ *% *.bak make.log MakeOut Makefile.dep \
$(PROG) $(PROG).errs $(PROG).1.html $(PROG)._man
	test -d win32 && (cd win32 ; make -f Makefile clean) ; true

distclean : clean
	$(RM) config.cache config.log
	$(RM)r autom4te.cache
	$(RM)r fr de
	$(RM) po/fr/AbacusT.mo po/de/AbacusT.mo
	test -d win32 && (cd win32 ; make -f Makefile distclean) ; true

clean.all : distclean
	$(RM) Makefile config.status*

autoconf :
	autoconf
	$(RM)r autom4te.cache

PACKAGE=$(VER)/$(WIDGET).h $(VER)/$(WIDGET)P.h \
$(VER)/xwin.h $(VER)/file.h $(VER)/timer.h $(VER)/rngs.h \
$(VER)/sound.h $(VER)/version.h \
$(VER)/pixmaps/mouse-l.xbm $(VER)/pixmaps/mouse-r.xbm \
$(VER)/pixmaps/*x*/$(wIDGET).* $(VER)/pixmaps/doc/Abacus0*.png \
$(VER)/bump.au $(VER)/move.au $(VER)/drip.au $(VER)/play.sh \
$(VER)/$(WIDGET)$(C) $(VER)/$(WIDGET)U$(C) $(VER)/$(WIDGET)C$(C) \
$(VER)/$(WIDGET)D$(C) $(VER)/$(WIDGET)M$(C) $(VER)/$(WIDGET)T$(C) \
$(VER)/$(WIDGET)F$(C) $(VER)/$(WIDGET)E$(C) $(VER)/$(P)$(wIDGET)$(C) \
$(VER)/xwin$(C) $(VER)/file$(C) $(VER)/timer$(C) $(VER)/rngs$(C) \
$(VER)/sound$(C) \
$(VER)/Imakefile $(VER)/$(PROG).man $(VER)/$(PROG).html \
$(VER)/$(WIDGET).ad $(VER)/$(WIDGET).ps $(VER)/*.xml \
$(VER)/configure $(VER)/Makefile.in $(VER)/configure.ac \
$(VER)/config.guess $(VER)/config.sub $(VER)/config.cygport \
$(VER)/install-sh $(VER)/mkinstalldirs \
$(VER)/AUTHORS $(VER)/ChangeLog $(VER)/COPYING $(VER)/NEWS \
$(VER)/INSTALL $(VER)/README $(VER)/TODO \
$(VER)/vms/README.vms $(VER)/vms/$(PROG).hlp \
$(VER)/vms/make.com $(VER)/vms/mmov.com \
$(VER)/vms/vms_amd.c $(VER)/vms/vms_amd.h $(VER)/vms/vms_mmov.c \
$(VER)/win32/Makefile $(VER)/win32/w$(wIDGET).rc \
$(VER)/win32/bump.wav $(VER)/win32/move.wav $(VER)/win32/drip.wav \
$(VER)/win32/w$(wIDGET).ini $(VER)/win32/$(wIDGET).ico\
$(VER)/po/AbacusT.pot $(VER)/po/de/AbacusT.po $(VER)/po/fr/AbacusT.po

zip :
	cd .. ; zip $(VER) $(PACKAGE)

pkzip :
	cd .. ; pkzip $(VER) $(PACKAGE)

tar :
	cd .. ; tar cvf $(VER).tar $(PACKAGE)

compress :
	cd .. ; tar cvf - $(PACKAGE) | compress > $(VER).tar.Z

gzip :
	cd .. ; tar cvf - $(PACKAGE) | gzip > $(VER).tar.gz

bzip2 :
	cd .. ; tar cvf - $(PACKAGE) | bzip2 > $(VER).tar.bz2

xz :
	cd .. ; tar cvf - $(PACKAGE) | xz > $(VER).tar.xz

dist : xz

dist.man : $(PROG).html vms/$(PROG).hlp

$(PROG).html : $(PROG).man
	if man2html $< > $(PROG).tmp; then\
		mv $(PROG).tmp $@;\
	else\
		rm -f $(PROG).tmp;\
	fi

# found man2hlp.sh in lynx source
vms/$(PROG).hlp : $(PROG).man
	man2hlp.sh $< > $(PROG).tmp 2> $(PROG)2.tmp
	if grep error $(PROG)2.tmp > /dev/null; then\
		cat $(PROG)2.tmp;\
		rm -f $(PROG).tmp $(PROG)2.tmp;\
	else\
		rm -f $(PROG)2.tmp;\
		mv $(PROG).tmp $@;\
	fi

html : $(PROG).html

hlp : vms/$(PROG).hlp

man :
	nroff -c -man $(PROG).man | $(MORE)

print:
	$(PRINT) $(HDRS) $(SRCS)

read :
	$(MORE) README

AUTOMAKE=/usr/share/automake-1.16

automake :
	cp -p ${AUTOMAKE}/config.guess .;\
	cp -p ${AUTOMAKE}/config.sub .;\
	cp -p ${AUTOMAKE}/install-sh .;\
	cp -p ${AUTOMAKE}/mkinstalldirs .

antic :
	antic *.h *.c */*/*.xpm */*/*.xbm */*.xbm */*.c */*.h

cppcheck :
	cppcheck -f -q *.c */*.c

lint :
	$(LINT) -ax -DLINT $(DEFS) $(XWIDGETINC) $(SRCS) $(XWIDGETLIBS)

indent :
	$(INDENT) $(HDRS) $(SRCS)

ICONS=\
	16x16/$(wIDGET).png\
	22x22/$(wIDGET).png\
	32x32/$(wIDGET).png\
	48x48/$(wIDGET).png

PNGS=$(ICONS:%.png=pixmaps/%.png)

pixmaps/%.png : pixmaps/%.xpm
	if grep None $< > /dev/null; then\
		sed -e 's/None/White/' $< | xpmtoppm |\
		pnmtopng -transparent 1,1,1 > $@;\
	else\
		xpmtoppm $< | pnmtopng > $@;\
	fi;\
	pngcrush -ow -rem allb -reduce $@

pixmap : $(PNGS)

DOC=\
	pixmaps/doc/Abacus001.png pixmaps/doc/Abacus002.png\
	pixmaps/doc/Abacus003.png pixmaps/doc/Abacus004.png\
	pixmaps/doc/Abacus005.png pixmaps/doc/Abacus006.png\
	pixmaps/doc/Abacus007.png pixmaps/doc/Abacus008.png\
	pixmaps/doc/Abacus009.png pixmaps/doc/Abacus010.png\
	pixmaps/doc/Abacus011.png pixmaps/doc/Abacus012.png

pixmaps/doc/Abacus001.png : Abacus.ps
	pstopnm Abacus.ps
	mkdir -p pixmaps/doc
	for i in Abacus*.ppm; do\
		j=`basename $$i .ppm`;\
		pnmtopng $$i > pixmaps/doc/$$j.png;\
		rm -f $$i;\
	done

doc : $(DOC)

msgfmt :
	for i in de fr; do\
		msgfmt --output-file=po/$$i/AbacusT.mo po/$$i/AbacusT.po;\
		mkdir -p $$i/LC_MESSAGES;\
		cp -p po/$$i/AbacusT.mo $$i/LC_MESSAGES;\
	done

install-msgfmt : msgfmt
	for i in de fr; do\
		cp -p po/fr/AbacusT.mo /usr/share/locale/fr/LC_MESSAGES;\
	done
